import typescript from 'rollup-plugin-typescript2';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

export default {
    input: './src/App.ts',
    output: {
        file: './serve/bundle.js',
        format: "iife",
        name: "SRP"
    },
    cache: true,
    sourcemap: true,
    plugins: [
        typescript(),
        resolve(),
        commonjs()
    ],
    onwarn: function (warning) {
        if (warning.code === 'THIS_IS_UNDEFINED') { return; }
        console.warn(warning.message);
    }
}
