# shiva-seed #

Get started with shiva🔱  
Take a look at this very simple example application written in Typescript. Behaviour, animation and presentation are all achieved with code; no markup or CSS.

## Install packages
```
npm i
``` 

## Develop
```
npm run dev
``` 
This command will launch a server at http://localhost:1337 showing the application.  
The npm script uses Rollup to compile the application to `/serve/bundle.js` and a sourcemap at `/serve/bundle.map.js`  
 It will also watch and reload the browser when changes to Typescript files in `/src` are detected.

## Production
```
npm run build
``` 
This will build a production version of the application, minified without source maps.

## Documentation
See [https://bitbucket.org/gabrielmccallin/shiva](https://bitbucket.org/gabrielmccallin/shiva)