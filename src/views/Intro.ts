import { Container } from 'shiva/Container';
import { Ease } from 'shiva/Ease';

import { Style } from './../styles/Style';

export class Intro extends Container {

    constructor() {
        super({
            type: "seed",
            display: "block",
            height: "100%",
            backgroundColor: Style.colours.verydarkgrey,
        });

        this.fromTo({
            duration: 3,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "1"
            }
        });

        const line = new Container({
            height: "1px",
            backgroundColor: Style.colours.lightgrey,
            width: "100%",
            position: "relative",
            top: "6.2rem",
            opacity: "0.3"
        });
        this.addChild(line);

        line.fromTo({
            ease: Ease.EaseOut,
            duration: 5,
            fromVars: {
                transform: "translateX(100%)"
            },
            toVars: {
                transform: "translateX(5rem)"
            }
        });

        const copy = new Container({
            text: "shiva-seed",
            style: Style.h1,
            position: "relative",
            top: "5rem"
        });
        this.addChild(copy);

        copy.fromTo({
            duration: 5,
            ease: Ease.EaseOut,
            fromVars: {
                transform: "translateX(0rem)",
                opacity: "0"
            },
            toVars: {
                transform: "translateX(5rem)",
                opacity: "1"
            }
        });
    }
}