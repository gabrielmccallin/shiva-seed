import { Container } from 'shiva/Container';

import { Style } from './../styles/Style';

export class BackgroundImage extends Container {

    constructor() {
        super({
            type: "bg-image",
            display: "block",
            width: "100%",
            height: "100%",
            backgroundImage: `url(${Style.backgroundImage})`,
            position: "absolute",
            top: "0",
            left: "0",
            zIndex: "2",
            backgroundSize: "cover"
        });

        this.fromTo({
            duration: 5,
            delay: 3,
            immediateRender: true,
            fromVars: {
                opacity: "0"
            },
            toVars: {
                opacity: "0.2"
            }
        });
    }
}