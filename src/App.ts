﻿import { RootContainer } from 'shiva/RootContainer';

import { Intro } from './views/Intro';
import { BackgroundImage } from './views/BackgroundImage';

class App extends RootContainer {

    constructor() {
        super();

        const intro = new Intro();
        this.addChild(intro);

        const bgImage = new BackgroundImage();
        this.addChild(bgImage);

    }
}

window.onload = () => {
    const app = new App();
};