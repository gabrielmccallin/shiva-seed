﻿export class Style {
    static colours = {
        lightgrey: "#ddd",
        lightpurple: "#9999cc",
        verydarkgrey: "#333333"
    };

    static fonts = {
        globalFont: "sans-serif"
    };

    static h1 = {
        fontSize: "1.2rem",
        fontFamily: Style.fonts.globalFont,
        color: Style.colours.lightgrey
    }

    static backgroundImage = "images/geometric.jpg";

}